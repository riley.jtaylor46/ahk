#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force

GP5HotkeyActive := True

Menu, Tray, Icon, GP5.ico

ToggleGP5Hotkeys()
{
    global GP5HotkeyActive
    if GP5HotkeyActive 
    {
        GP5HotkeyActive := False
        Menu, Tray, Icon, GP5.ico
        Tooltip, % "Enabled GP5 Pro Hotkeys"
        Sleep 500
        Tooltip,
    }
    else
    {
        GP5HotkeyActive := True
        Menu, Tray, Icon, GP5-off.ico
        Tooltip, % "Disabled GP5 Pro Hotkeys"
        Sleep 500
        Tooltip,
    }
}

DetectImage(image)
{
    ImageSearch, FoundX, FoundY, 0,0, 1280,720, %image%
    if (ErrorLevel=0)
    {
        return 1
    }
    return 0
}

; detects if guitar pro is active and tooltips are enabled
gp5active(GP5HotkeyActive)
{
    if WinActive("ahk_exe GP5.exe") = 0x0
        return False
    if !GP5HotkeyActive
        return False
    if DetectImage("quick-access-star.png")
        return False
    if DetectImage("gp5-cancel.png")
        return False
    return True
}

^!g:: ToggleGP5Hotkeys()
; Pause::Reload

; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
; Guitar Pro 5 Shortcuts
;
#If gp5active(GP5HotkeyActive)
 

    F2:: Send {F10}   ; Mixer 

    F3:: ; Open Transpose window
        Send {Alt down}{t}{t}{Alt up}{Enter}{t}
        Return

    F4:: Send ^+{Ins} ; New Track/Instrument

    F5:: Send {F7} ; Change Instrument

    m::              ; midi input toggle
        Send {Alt down}{s}{m}{m}{Alt up}{Enter}
        ToolTip, Toggled MIDI Input
        Sleep 1000
        ToolTip,
        Return

    q:: Send {NumpadSub}		; Longer Note
    w:: Send {NumpadAdd}		; Shorter Note
    e:: Send {Alt down}{n}{d}{d}{Enter}{-}{Enter}{Alt up} ; Triplet
    t:: Send {l}                ; Tie Note
    ^t:: Send ^{l}

    d:: ; dots note
        Send {Alt down}{n}{d}{d}{Right}{d}{Alt up}
        Return

    z:: Send {Ctrl down}{Delete}{Ctrl up}

    v:: Send {i}

    +q:: Send {Alt down}{n}{u}{u}{Enter}{Alt up}
    +w:: Send {Alt down}{n}{d}{d}{d}{d}{d}{Enter}{Alt up}

    +t:: Send {Alt down}{v}{Enter}{Alt up}

    +a::
        Send {Alt down}{e}{e}{Alt up}{Enter}
        Sleep 30
        Send {s}{Right}{s}{s}{Enter}
        Return
    +s::
        Send {Alt down}{e}{e}{Alt up}{Enter}
        Sleep 30
        Send {s}{Right}{s}{s}{s}{Enter}
        Return
    +d::
        Send {Alt down}{e}{e}{Alt up}{Enter}
        Sleep 30
        Send {s}{Right}{s}{s}{s}{s}{Enter}
        Return
    +f::
        Send {Alt down}{e}{e}{Alt up}{Enter}
        Sleep 30
        Send {s}{Right}{s}{s}{s}{s}{s}{Enter}
        Return

    ; Open midi select screen
    +M::
        Send {Alt down}{o}{a}{Alt up}
        Return

    ; Guitar Pro Hotkey Help GUI
    F1::
        Gui, New

        Gui, Font, Bold s16
        Gui, Add, Text, x10, Guitar Pro Hotkeys Help
        Gui, Font

        KeyInfo := []
        KeyInfo.Push("F1`nHelp")
        KeyInfo.Push("F2`nMixer")
        KeyInfo.Push("F3`nTranspose")
        KeyInfo.Push("F4`nNew Track")
        KeyInfo.Push("F5`nChange Instrument")
        KeyInfo.Push("F6`nInstrument Properties")

        KeyInfo.Push("ROW")

        KeyInfo.Push("Q`nLonger Beat")
        KeyInfo.Push("W`nShorter Beat")
        KeyInfo.Push("E`nTriplet")
        KeyInfo.Push("R`nRest")
        KeyInfo.Push("T`nTie Note")

        KeyInfo.Push("ROW")

        KeyInfo.Push("A`nChord Menu")
        KeyInfo.Push("S`nLegato Slide")
        KeyInfo.Push("D`nDot Note")
        KeyInfo.Push("F`nFade In")
        KeyInfo.Push("G`nGrace Note")

        KeyInfo.Push("ROW")

        KeyInfo.Push("Z`nDelete Note")
        KeyInfo.Push("X`nDead Note")
        KeyInfo.Push("C`nCopy Note")
        KeyInfo.Push("V`nLet Ring")
        KeyInfo.Push("B`nBend")

        KeyInfo.Push("ROW")
        KeyInfo.Push("ROW")

        KeyInfo.Push("Shift+Q`n+1 Semitone")
        KeyInfo.Push("Shift+W`n-1 Semitone")
        KeyInfo.Push("")
        KeyInfo.Push("")
        KeyInfo.Push("Shift+T`nMultitrack View")

        KeyInfo.Push("ROW")

        KeyInfo.Push("Shift+A`nSlide In From Below")
        KeyInfo.Push("Shift+S`nSlide In From Above")
        KeyInfo.Push("Shift+D`nSlide Out Downwards")
        KeyInfo.Push("Shift+F`nSlide Out Upwards")
        
        x := 0
        y := 0
        box_height := 50
        box_width := 100


        Gui, Font, s10
        for index, info in KeyInfo
        {
            if (info = "ROW") {
                x := 0
                y := y + 1
                Continue
            }

            x_size := x * (box_width + 10) + 10
            y_size := y * (box_height + 10) + 50

            Gui, Add, Text, % "yp10 0x1000 Center  x" x_size " y" y_size " w" box_width " h" box_height "" Border, % info

            x := x + 1
        }
        Gui, Font
        
        Gui, Show, , Guitar Pro Hotkey Help

        Return
#If
