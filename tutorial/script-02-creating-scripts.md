# Creating Scripts

I swear we'll get to actual scripting soon, but I really need to show you how to manage your scripts. Once AutoHotkey is installed, it gives us a new file type to create a `New AutoHotkey Script`. It then creates a `dot AHK` file. That's the file extension of the script. Although a script is just a simple text file, when the file has a `dot AHK` extension AutoHotkey knows to run it as a script.

Well, let's make our first script then. Oh gosh, this is exciting! Right Click in the file explorer and then choose New, and then New Auto Hotkey Script. Now we can edit it in VS Code by Right CLicking on it and selecting "Open with Code".

The world of possibilites are open to us with a blank text area, but let's keep it simple for now. I want to teach you the process behind running scripts and how to manage the scripts you have running. We'll go over what the actual script means in the next videos.

> F1::
>   Send, Hello World!
>   Return

Typically in programming languages, the first program you write is one that outputs "Hello World" to the screen. Since AutoHotkey is about controlling your computer, I thought I'd make our first program type `Hello World!` for you.

This short script types `Hello World` and an exclamation point whenever you press `F1`. Once you've copied it in, go ahead and save, and let's run the script. Double clicking on the script will run it with AutoHotkey. You'll see it running in the system tray. I like to have my scripts always showing so I know whats active. You can tell Windows to do that by draggint it down to the other icons. We'll be refrencing the open scripts constantly while developing scripts.

Let's make sure our script works. If we go back to our text editor and press F1 it should type in `Hello World!`. Looks like it works!

Next I want to show how to manage your scripts. AutoHotkey can be a deadly beast when your script gets out of control, so knowing how to stop the scripts are extremely important. When you hover over your script in the taskbar it will say the name of the script. This might be important when you have many scripts running since they all share the same icon. Note that if your script is not here it may be hidden inside here. When you right click on the script there are some management options available. 

Pause or Suspend Hotkeys will stop the script but keep it running.  You'll notice that the icon changes to red, and that the F1 hotkey won't work anymore. Right click again to unpause the script. 

`Reload This Script` is something beginners often miss. When you save your script, you have to reload it so that AutoHotkey knows about your changes. Let's try doing that now. I'll change the text to something else.

> F1::
>   Send, The script was reloaded.
>   Return

Now when I go and reload the script, the F1 command will be updated. Looks like it works!

The last one you need to know is Exit. This completely stops the script, and the hotkeys will no longer be active. In fact if I go ahead and click Exit now. 

[stop the script half way through talking and do the TV shutdown thing exactly once i click exit]
[ might be funny / need to change the last sentence]




# NOTES
Going back to 


1. make a hotkey
1. make it send hello
save
run script
drag it down part
try tio see it work
lets change it to hello2 or w/e
now we need to reload
Remember! If you make changes to your script and save you must reload
it works again
This was the basics yay!